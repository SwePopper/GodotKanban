tool
extends EditorPlugin

var board = null

func _exit_tree():
	if board:
		get_editor_interface().get_editor_viewport().remove_child(board)
		board.queue_free()

func make_visible(visible):
	if visible:
		board = preload("res://addons/swepopper.kanban/board.tscn").instance()
		get_editor_interface().get_editor_viewport().add_child(board)
	elif board:
		board.save_kanban_board()
		get_editor_interface().get_editor_viewport().remove_child(board)
		board.queue_free()
		board = null

func has_main_screen():
	return true
	
func get_plugin_name():
    return "Kanban"
	
func get_plugin_icon():
	return preload("res://addons/swepopper.kanban/kanban.png")
