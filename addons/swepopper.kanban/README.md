# kanban
A plugin for the Godot Game Engine, to add kanban project planning.

# Howto
Press "Add Feature" to add new feature/story
Drag and drop the new feature/store to the different columns
Change feature/story information in Feature Info

# Todo
Add drag and drop icon
Add more parameters to features (dates, task list, texture references and more)
Better ui layout
Put feature info stuff in Inspector?
Add network sync feature for collab?