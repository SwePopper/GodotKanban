tool
extends PanelContainer

var column_id = 0

func can_drop_data(position, data):
	return _get_board().has_feature(data)
	
func drop_data(position, data):
	_get_board().move_feature(data, column_id)
	
func _get_board():
	var b = get_parent()
	while b != null && b.name != "Board":
		b = b.get_parent()
	return b
