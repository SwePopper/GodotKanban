tool
extends Button

var _unique_id

func get_drag_data(position):
	set_drag_preview(duplicate())
	return _unique_id
	
func set_title(title):
	text = title

func set_id(id):
	_unique_id = id
	name = id
	
func _pressed():
	_get_board().focus_feature(_unique_id)
	
func _get_board():
	var b = get_parent()
	while b != null && b.name != "Board":
		b = b.get_parent()
	return b