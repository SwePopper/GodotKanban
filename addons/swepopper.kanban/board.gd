tool
extends PanelContainer

enum ColumnID { PLANNED, DEVELOPMENT, TESTING, DONE, COLUMN_COUNT }
var ColumnTitles = [ "Planned", "Development", "Testing", "Done" ]

var save_file = "res://kanban.json"
var feature_info

var column_pref = preload("res://addons/swepopper.kanban/column.tscn")
var feature_pref = preload("res://addons/swepopper.kanban/feature.tscn")

var _features = {}

func _ready():
	for i in range(COLUMN_COUNT):
		_init_column(i, ColumnTitles[i])
	
	feature_info = find_node("Info")
	feature_info.board = self
	feature_info.find_node("Delete").connect("pressed", self, "delete_feature")
	find_node("AddFeature").connect("pressed", self, "add_feature")
	find_node("Save").connect("pressed", self, "save_kanban_board")
	load_kanban_board()
	
func _init_column(column_id, title):
	var column = _get_column(column_id)
	column.column_id = column_id
	column.find_node("Title").text = title

func _get_column(column_id):
	var column_container = find_node("ColumnContainer")
	var column = column_container.find_node(ColumnTitles[column_id])	
	if column == null:
		column = column_pref.instance()
		column_container.add_child(column)
		column.owner = column_container
		column.name = ColumnTitles[column_id]
		
		var separator = VSeparator.new()
		column_container.add_child(separator)
		separator.owner = column_container
	return column

func _get_unique_id():
	for i in range(100000):
		var id = String(i)
		if _features.has(id):
			continue
		return id
		
func has_feature(id):
	return _features.has(id)
	
func move_feature(id, column_id):
	if _features.has(id):
		if _features[id].column == column_id:
			return

		var feature = find_node(id)
		
		var column = _get_column(_features[id].column)
		if column:
			if feature == null:
				feature = column.find_node(id)
			feature.owner.remove_child(feature)
		
		column = _get_column(column_id).find_node("FeatureContainer")
		if column:
			_features[id].column = column_id
			column.add_child(feature)
			feature.owner = column
	pass

func create_feature(column = null):
	if column == null:
		column = _get_column(PLANNED)
	
	var feature_container = column.find_node("FeatureContainer")
		
	var feature = feature_pref.instance()
	feature_container.add_child(feature)
	feature.owner = feature_container
	return feature
	
func add_feature():
	var feature = create_feature()
	var feature_data = {}
	feature_data.id = _get_unique_id()
	feature_data.title = "Feature Name"
	feature_data.description = "Description"
	feature_data.column = PLANNED
	feature_data.time_created = OS.get_unix_time()
	_features[feature_data.id] = feature_data
	
	feature.set_id(feature_data.id)
	feature.set_title(feature_data.title)
	
	feature_info.display_feature(feature_data)

func delete_feature():
	if feature_info.currentFeature:
		print("delete feature ", feature_info.currentFeature)
		find_node(feature_info.currentFeature.id).queue_free()
		_features.erase(feature_info.currentFeature.id)
		feature_info.display_feature(null)
	pass

func update_feature(feature_data):
	_features[feature_data.id] = feature_data
	var feature = find_node(feature_data.id)
	if feature:
		feature.set_title(feature_data.title)
	
func focus_feature(id):
	if _features.has(id):
		feature_info.display_feature(_features[id])
	
func load_kanban_board():
	var file = File.new()
	var status = file.open(save_file, File.READ)
	_features = {}
	if status == OK:
		var result = JSON.parse(file.get_as_text())
		if result.error == OK:
			_features = result.result
			
	var features = _features.values()
	for feature_data in features:
		if feature_data.has("id") && feature_data.has("title"):
			var feature = create_feature(_get_column(feature_data.column))
			feature.set_id(feature_data.id)
			feature.set_title(feature_data.title)

func save_kanban_board():
	var file = File.new()
	var status = file.open(save_file, File.WRITE)
	if status == OK:
		file.store_string(JSON.print(_features))
		file.close()
	else:
		printerr("FAILED TO OPEN KANBAN TO SAVE: ", status);
	pass
