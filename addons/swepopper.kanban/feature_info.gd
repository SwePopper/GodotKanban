tool
extends PanelContainer

var title
var description
var deleteButton
var board

var currentFeature = null

func _ready():
	title = find_node("Title")
	description = find_node("Description")
	deleteButton = find_node("Delete")
	
	display_feature(null)
	
	title.connect("text_changed", self, "changed_title")
	description.connect("text_changed", self, "changed_description")

func display_feature(feature):
	currentFeature = feature
	if feature:
		title.text = currentFeature.title
		description.text = currentFeature.description
		deleteButton.disabled = false
	else:
		# disable
		title.text = ""
		description.text = ""
		deleteButton.disabled = true
		
func changed_title(text = ""):
	if currentFeature != null:
		currentFeature.title = title.text
		board.update_feature(currentFeature)

func changed_description(text = ""):
	if currentFeature != null:
		currentFeature.description = description.text
		board.update_feature(currentFeature)
